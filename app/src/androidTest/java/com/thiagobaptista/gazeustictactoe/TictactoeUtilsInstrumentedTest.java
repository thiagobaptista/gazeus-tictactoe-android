package com.thiagobaptista.gazeustictactoe;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.thiagobaptista.gazeustictactoe.model.Cell;
import com.thiagobaptista.gazeustictactoe.util.TictactoeUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class TictactoeUtilsInstrumentedTest
{
    @Test
    public void should_be_possible_to_get_the_Views_id_from_a_row_and_a_column()
    {
        // Context of the app under test.
        Context context = InstrumentationRegistry.getTargetContext();
        Random dice = new Random();

        int expectedRow = dice.nextInt(3);
        int expectedColumn = dice.nextInt(3);
        String resourceName = "cell_" + expectedRow + "_" + expectedColumn;
        int expectedId = context.getResources().getIdentifier(resourceName, "id", context.getPackageName());

        assertEquals(expectedId, TictactoeUtils.getResourceIdFromCell(new Cell(expectedRow, expectedColumn), context));
    }

    @Test
    public void should_be_possible_to_get_a_Cell_object_from_the_Views_id()
    {
        // Context of the app under test.
        Context context = InstrumentationRegistry.getTargetContext();
        Random dice = new Random();

        int expectedRow = dice.nextInt(3);
        int expectedColumn = dice.nextInt(3);
        String resourceName = "cell_" + expectedRow + "_" + expectedColumn;
        int expectedId = context.getResources().getIdentifier(resourceName, "id", context.getPackageName());
        Cell expectedCell = new Cell(expectedRow, expectedColumn);

        assertEquals(expectedCell, TictactoeUtils.getCellFromId(expectedId, context));
    }
}
