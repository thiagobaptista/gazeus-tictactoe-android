package com.thiagobaptista.gazeustictactoe.util;

import android.content.Context;
import android.widget.Toast;

import com.thiagobaptista.gazeustictactoe.model.Cell;

public class TictactoeUtils
{
    private static String getCellIdString(Cell cell)
    {
        return "cell_" + cell.getRow() + "_" + cell.getColumn();
    }

    private static int getResourceId(String name, Context context)
    {
        return context.getResources().getIdentifier(name, "id", context.getPackageName());
    }

    /**
     * Gets the hexadecimal id from the R class corresponding to the cell view's id {@link String}.
     *
     * @param cell the cell
     * @param context the Context
     * @return the R class numeric id
     */
    public static int getResourceIdFromCell(Cell cell, Context context)
    {
        String name = getCellIdString(cell);
        return getResourceId(name, context);
    }

    private static String getCellNameFromId(int id, Context context)
    {
        return context.getResources().getResourceName(id);
    }

    /**
     * Gets a Cell object pertaining to a specific cell view, based on its hexadecimal id from the R class.
     *
     * @param id the View's numeric id from the R class
     * @param context the Context
     * @return the corresponding Cell object
     */
    public static Cell getCellFromId(int id, Context context)
    {
        String cellId = getCellNameFromId(id, context);
        String[] splits = cellId.split("[_]");

        int row = Integer.parseInt(splits[1]);
        int column = Integer.parseInt(splits[2]);

        return new Cell(row, column);
    }

    /**
     * Helper method to show a toast with a specified text for a {@link Toast#LENGTH_LONG} duration.
     *
     * @param text the text to be shown
     * @param context the Context
     */
    public static void longToast(String text, Context context)
    {
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        toast.show();
    }
}
