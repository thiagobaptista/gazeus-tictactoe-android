package com.thiagobaptista.gazeustictactoe.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The GameBoard represents the game logic itself. It is the "engine" that drives
 * the game.
 *
 * It consists mainly of a series of game states, such as the {@link GameState} proper, a
 * turn number counter, the current {@link Player} and the eventual winner, and a grid of
 * cells representing the game board. This grid implemented via a {@link Map} of keys made
 * by {@link Cell} objects tied to {@link Player} values, representing the player's markings.
 *
 * It's main public API is the method {@link GameBoard#makeAMove(Cell)}, which takes a {@link Cell}
 * as parameter and marks it on the grid corresponding to the current player. The method then checks
 * for endgame states and updates the GameBoard accordingly.
 *
 * The GameBoard also contains a listener interface, {@link GameBoardListener}, which acts as the
 * means of communicating with the view.
 */
public class GameBoard
{
    private Player currentPlayer = Player.X;
    private Player winner = Player.NONE;

    private GameState currentState = GameState.IN_PROGRESS;

    private int currentTurn = 0;

    private Map<Cell, Player> cellGrid = new HashMap<>();

    private GameBoardListener listener;

    public GameBoard()
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                cellGrid.put(new Cell(i, j), Player.NONE);
            }
        }
    }

    public void makeAMove(Cell cell)
    {
        // Checks whether the game is still running
        if (currentState == GameState.GAME_OVER)
        {
            return;
        }

        // Checks whether this cell is already marked
        if ( isAlreadyMarked(cell) )
        {
            return;
        }

        // Turn counter goes up
        currentTurn++;

        // Marks the cell
        cellGrid.put(cell, currentPlayer);
        listener.onCellMarked(cell, currentPlayer);

        // Checks for endgame conditions
        checkRowComplete();
        checkColumnComplete();
        checkDownwardDiagonalComplete();
        checkUpwardDiagonalComplete();
        checkDraw();

        // Now its the other player's turn
        switchPlayers();
    }

    private boolean isAlreadyMarked(Cell cell)
    {
        return cellGrid.get(cell) != Player.NONE;
    }

    private void checkRowComplete()
    {
        for (int i = 0; i < 3; i++)
        {
            Cell cell1 = new Cell(i, 0);
            Cell cell2 = new Cell(i, 1);
            Cell cell3 = new Cell(i, 2);
            Player mark1 = cellGrid.get(cell1);
            Player mark2 = cellGrid.get(cell2);
            Player mark3 = cellGrid.get(cell3);

            if (mark1 != Player.NONE
                    && mark1 == mark2
                    && mark1 == mark3)
            {
                currentState = GameState.GAME_OVER;
                winner = mark1;

                List<Cell> winningCells = new ArrayList<>();
                winningCells.add(cell1);
                winningCells.add(cell2);
                winningCells.add(cell3);
                listener.onPlayerWin(winner, winningCells);
            }
        }
    }

    private void checkColumnComplete()
    {
        for (int i = 0; i < 3; i++)
        {
            Cell cell1 = new Cell(0, i);
            Cell cell2 = new Cell(1, i);
            Cell cell3 = new Cell(2, i);
            Player mark1 = cellGrid.get(cell1);
            Player mark2 = cellGrid.get(cell2);
            Player mark3 = cellGrid.get(cell3);
            if (mark1 != Player.NONE
                    && mark1 == mark2
                    && mark1 == mark3)
            {
                currentState = GameState.GAME_OVER;
                winner = mark1;

                List<Cell> winningCells = new ArrayList<>();
                winningCells.add(cell1);
                winningCells.add(cell2);
                winningCells.add(cell3);
                listener.onPlayerWin(winner, winningCells);
            }
        }
    }

    private void checkDownwardDiagonalComplete()
    {
        Cell cell1 = new Cell(0, 0);
        Cell cell2 = new Cell(1, 1);
        Cell cell3 = new Cell(2, 2);
        Player mark1 = cellGrid.get(cell1);
        Player mark2 = cellGrid.get(cell2);
        Player mark3 = cellGrid.get(cell3);
        if (mark1 != Player.NONE
                && mark1 == mark2
                && mark1 == mark3)
        {
            currentState = GameState.GAME_OVER;
            winner = mark1;

            List<Cell> winningCells = new ArrayList<>();
            winningCells.add(cell1);
            winningCells.add(cell2);
            winningCells.add(cell3);
            listener.onPlayerWin(winner, winningCells);
        }
    }

    private void checkUpwardDiagonalComplete()
    {
        Cell cell1 = new Cell(2, 0);
        Cell cell2 = new Cell(1, 1);
        Cell cell3 = new Cell(0, 2);
        Player mark1 = cellGrid.get(cell1);
        Player mark2 = cellGrid.get(cell2);
        Player mark3 = cellGrid.get(cell3);
        if (mark1 != Player.NONE
                && mark1 == mark2
                && mark1 == mark3)
        {
            currentState = GameState.GAME_OVER;
            winner = mark1;

            List<Cell> winningCells = new ArrayList<>();
            winningCells.add(cell1);
            winningCells.add(cell2);
            winningCells.add(cell3);
            listener.onPlayerWin(winner, winningCells);
        }
    }

    private void checkDraw()
    {
        // If this was the 9th turn, then it's a draw
        if (currentTurn >= 9)
        {
            currentState = GameState.GAME_OVER;
            listener.onDraw();
        }
    }

    private void switchPlayers()
    {
        if (currentPlayer == Player.X)
        {
            currentPlayer = Player.O;
        }
        else
        {
            currentPlayer = Player.X;
        }

        listener.onPlayerSwitched(currentPlayer);
    }

    public Player checkCell(Cell cell)
    {
        return cellGrid.get(cell);
    }

    public Player getCurrentPlayer()
    {
        return currentPlayer;
    }

    public GameState getCurrentState()
    {
        return currentState;
    }

    public int getCurrentTurn()
    {
        return currentTurn;
    }

    public Player getWinner()
    {
        return winner;
    }

    public void setGameBoardListener(GameBoardListener listener)
    {
        this.listener = listener;
    }

    /**
     * Enum listing all possible player states. It represents both the current player
     * in a move and the mark done to a cell by its play.
     *
     * Alongside a representation for X and O, this enum also has a NONE field, since
     * there can be a state where both:
     * 1) a cell is marked by no player at all
     * 2) there is no winner, either because the game is still ongoing
     *      or because the game ended in a draw
     */
    public enum Player
    {
        X, O, NONE
    }

    /**
     * Enum representing the possible game states. The game start in the IN_PROGRESS state, and
     * ends in the GAME_OVER state.
     */
    public enum GameState
    {
        IN_PROGRESS,
        GAME_OVER
    }

    /**
     * Contains a list of callback methods that allows for communication between the game engine
     * and the view layer.
     */
    public interface GameBoardListener
    {
        void onCellMarked(Cell cell, Player mark);

        void onPlayerSwitched(Player currentPlayer);

        void onPlayerWin(Player winner, List<Cell> winningCells);

        void onDraw();
    }
}

