package com.thiagobaptista.gazeustictactoe;

import com.thiagobaptista.gazeustictactoe.model.Cell;
import com.thiagobaptista.gazeustictactoe.model.GameBoard;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static com.thiagobaptista.gazeustictactoe.model.GameBoard.*;

public class GameBoardListenerUnitTest
{
    private GameBoard gameBoard;

    private MockListener listener;

    @Before
    public void setup()
    {
        resetGameBoard();
    }

    private void resetGameBoard()
    {
        gameBoard = new GameBoard();
        listener = new MockListener();
        gameBoard.setGameBoardListener(listener);
    }

    @Test
    public void should_the_OnCellMarked_callback_return_the_cell()
    {
        Cell cell = new Cell(0, 0);
        gameBoard.makeAMove(cell);

        Cell returnedCell = listener.getLastReturnedCell();
        assertEquals(cell, returnedCell);
    }

    @Test
    public void should_the_OnCellMarked_callback_return_the_player()
    {
        Cell cell = new Cell(0, 0);
        gameBoard.makeAMove(cell);

        Player returnedPlayer = listener.getLastReturnedPlayer();
        assertEquals(Player.X, returnedPlayer);
    }

    @Test
    public void should_there_be_a_callback_for_when_the_player_switches()
    {
        gameBoard.makeAMove(new Cell(0, 0));

        assertEquals(Player.O, listener.switchedPlayer());
    }

    @Test
    public void should_there_be_a_callback_for_when_the_game_is_won()
    {
        List<Cell> xWinningCells = new ArrayList<>();
        xWinningCells.add(new Cell(2, 0));
        xWinningCells.add(new Cell(1, 1));
        xWinningCells.add(new Cell(0, 2));
        simulatePlayerXWin();
        assertEquals(Player.X, listener.getWinner());
        assertEquals(xWinningCells, listener.getReturnedWinningCells());

        List<Cell> oWinningCells = new ArrayList<>();
        oWinningCells.add(new Cell(0, 0));
        oWinningCells.add(new Cell(1, 0));
        oWinningCells.add(new Cell(2, 0));
        resetGameBoard();
        simulatePlayerOWin();
        assertEquals(Player.O, listener.getWinner());
        assertEquals(oWinningCells, listener.getReturnedWinningCells());
    }

    private void simulatePlayerXWin()
    {
        // X plays
        gameBoard.makeAMove(new Cell(2, 0));
        // O plays
        gameBoard.makeAMove(new Cell(0, 0));

        // X plays
        gameBoard.makeAMove(new Cell(1, 1));
        // O plays
        gameBoard.makeAMove(new Cell(1, 2));

        // X plays
        gameBoard.makeAMove(new Cell(0, 2));
    }

    private void simulatePlayerOWin()
    {
        // X plays
        gameBoard.makeAMove(new Cell(0, 2));
        // O plays
        gameBoard.makeAMove(new Cell(0, 0));

        // X plays
        gameBoard.makeAMove(new Cell(1, 2));
        // O plays
        gameBoard.makeAMove(new Cell(1, 0));

        // X plays
        gameBoard.makeAMove(new Cell(1, 1));
        // O plays
        gameBoard.makeAMove(new Cell(2, 0));
    }

    @Test
    public void should_there_be_a_callback_for_when_the_game_is_a_draw()
    {
        simulateDraw();
        assert listener.wasADraw();
    }

    private void simulateDraw()
    {
        gameBoard.makeAMove(new Cell(0, 0));
        gameBoard.makeAMove(new Cell(1, 0));
        gameBoard.makeAMove(new Cell(0, 1));
        gameBoard.makeAMove(new Cell(0, 2));
        gameBoard.makeAMove(new Cell(1, 1));
        gameBoard.makeAMove(new Cell(2, 1));
        gameBoard.makeAMove(new Cell(1, 2));
        gameBoard.makeAMove(new Cell(2, 2));
        gameBoard.makeAMove(new Cell(2, 0));
    }

    private class MockListener implements GameBoardListener
    {
        private Cell returnedCell;

        private List<Cell> returnedWinningCells;

        private Player returnedPlayer;
        private Player switchedPlayer;
        private Player winner;

        private boolean draw = false;

        @Override
        public void onCellMarked(Cell cell, Player mark)
        {
            returnedCell = cell;
            returnedPlayer = mark;
        }

        @Override
        public void onPlayerSwitched(Player currentPlayer)
        {
            switchedPlayer = currentPlayer;
        }

        @Override
        public void onPlayerWin(Player winner, List<Cell> winningCells)
        {
            this.winner = winner;
            returnedWinningCells = winningCells;
        }

        @Override
        public void onDraw()
        {
            draw = true;
        }

        public Cell getLastReturnedCell()
        {
            return returnedCell;
        }

        public List<Cell> getReturnedWinningCells()
        {
            return returnedWinningCells;
        }

        public Player getLastReturnedPlayer()
        {
            return returnedPlayer;
        }

        public Player switchedPlayer()
        {
            return switchedPlayer;
        }

        public Player getWinner()
        {
            return winner;
        }

        public boolean wasADraw()
        {
            return draw;
        }
    }
}
