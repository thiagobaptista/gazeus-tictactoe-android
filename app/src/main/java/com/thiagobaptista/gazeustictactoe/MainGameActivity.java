package com.thiagobaptista.gazeustictactoe;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.thiagobaptista.gazeustictactoe.model.Cell;
import com.thiagobaptista.gazeustictactoe.model.GameBoard;
import com.thiagobaptista.gazeustictactoe.util.TictactoeUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainGameActivity extends AppCompatActivity implements View.OnClickListener, GameBoard.GameBoardListener
{
    private GameBoard gameBoard;

    private TextView currentPlayerView;
    private TextView buttonReset;

    private View gameDrawOverlay;

    private Map<Cell, TextView> cellViews = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        resetGameAndUI();
    }

    private void resetGameAndUI()
    {
        setContentView(R.layout.activity_main_game);

        // Sets up the game engine
        gameBoard = new GameBoard();
        gameBoard.setGameBoardListener(this);

        // Sets up all the views representing the various board cells
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                Cell cell = new Cell(i, j);

                int id = TictactoeUtils.getResourceIdFromCell(cell, this);
                TextView view = findViewById(id);
                cellViews.put(cell, view);

                if (view != null)
                {
                    view.setOnClickListener(this);
                }
            }
        }

        // Sets up the current player/game over text view
        currentPlayerView = findViewById(R.id.text_current_player);

        // Sets up the game draw overlay
        gameDrawOverlay = findViewById(R.id.view_velha_overlay);
        if (gameDrawOverlay != null)
        {
            gameDrawOverlay.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    gameDrawOverlay.setVisibility(View.GONE);
                }
            });
        }

        // Sets up the reset button
        buttonReset = findViewById(R.id.button_reset);
        if (buttonReset != null)
        {
            buttonReset.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    resetGameAndUI();
                }
            });
        }
    }

    @Override
    public void onClick(View view)
    {
        Cell cell = TictactoeUtils.getCellFromId(view.getId(), this);
        gameBoard.makeAMove(cell);
    }

    @Override
    public void onCellMarked(Cell cell, GameBoard.Player mark)
    {
        TextView cellView = cellViews.get(cell);
        if (cellView != null)
        {
            cellView.setText(mark.toString());
            cellView.setTextColor(Color.WHITE);
        }
    }

    @Override
    public void onPlayerSwitched(GameBoard.Player currentPlayer)
    {
        if (currentPlayerView != null
                && gameBoard.getCurrentState() != GameBoard.GameState.GAME_OVER)
        {
            currentPlayerView.setText("Current player: " + currentPlayer);
        }
    }

    @Override
    public void onPlayerWin(GameBoard.Player winner, List<Cell> winningCells)
    {
        if (currentPlayerView != null)
        {
            String text = "Game Over. Player " + winner + " won!";
            currentPlayerView.setText(text);
        }

        for (Cell cell : cellViews.keySet())
        {
            if (winningCells.contains(cell))
            {
                TextView view = cellViews.get(cell);
                view.setBackgroundColor(Color.RED);
            }
        }
    }

    @Override
    public void onDraw()
    {
        if (currentPlayerView != null)
        {
            String text = "Game Over. It's a draw...";
            currentPlayerView.setText(text);
        }

        if (gameDrawOverlay != null)
        {
            gameDrawOverlay.setVisibility(View.VISIBLE);

            MediaPlayer mp = MediaPlayer.create(this, R.raw.itsadraw);
            mp.start();
        }
    }
}
