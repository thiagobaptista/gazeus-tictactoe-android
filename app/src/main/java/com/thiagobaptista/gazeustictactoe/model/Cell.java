package com.thiagobaptista.gazeustictactoe.model;

import java.util.Objects;

/**
 * A plain-old Java object representing a tic-tac-toe board grid cell.
 */
public class Cell
{
    private int row;
    private int column;

    public Cell(int row, int column)
    {
        this.row = row;
        this.column = column;
    }

    public int getRow()
    {
        return row;
    }

    public int getColumn()
    {
        return column;
    }

    @Override
    public boolean equals(Object other)
    {
        if (this == other)
        {
            return true;
        }
        if (other == null || getClass() != other.getClass())
        {
            return false;
        }
        Cell cell = (Cell) other;
        return row == cell.row &&
                column == cell.column;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(row, column);
    }

    @Override
    public String toString()
    {
        return "[Row = " + row + ", Column = " + column + "]";
    }
}
