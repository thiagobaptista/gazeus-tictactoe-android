package com.thiagobaptista.gazeustictactoe;

import com.thiagobaptista.gazeustictactoe.model.Cell;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class CellUnitTest
{
    @Test
    public void should_a_Cell_have_a_row_and_a_column()
    {
        Random dice = new Random();
        int row = dice.nextInt();
        int column = dice.nextInt();

        Cell cell = new Cell(row, column);

        assertEquals(row, cell.getRow());
        assertEquals(column, cell.getColumn());
    }

    @Test
    public void should_two_equal_Cells_be_equal()
    {
        Random dice = new Random();
        int row = dice.nextInt(3);
        int column = dice.nextInt(3);

        Cell cell1 = new Cell(row, column);
        Cell cell2 = new Cell(row, column);

        assertEquals(cell1, cell2);
    }

    @Test
    public void should_two_different_Cells_be_not_equal()
    {
        Random dice = new Random();
        int row1 = dice.nextInt(3);
        int column1 = dice.nextInt(3);
        int row2 = generateDifferentRandomNumber(dice, row1);
        int column2 = generateDifferentRandomNumber(dice, column1);

        Cell cell1 = new Cell(row1, column1);
        Cell cell2 = new Cell(row2, column2);

        assertNotEquals(cell1, cell2);
    }

    private int generateDifferentRandomNumber(Random random, int differentFrom)
    {
        int number = random.nextInt(3);
        if (number == differentFrom)
        {
            return generateDifferentRandomNumber(random, differentFrom);
        }
        return number;
    }

    @Test
    public void should_correctly_implement_ToString()
    {
        Random dice = new Random();
        int row = dice.nextInt(3);
        int column = dice.nextInt(3);

        String expectedString = "[Row = " + row + ", Column = " + column + "]";
        Cell cell = new Cell(row, column);

        assertEquals(expectedString, cell.toString());
    }
}
