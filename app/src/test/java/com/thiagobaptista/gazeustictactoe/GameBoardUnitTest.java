package com.thiagobaptista.gazeustictactoe;

import org.junit.Before;
import org.junit.Test;

import com.thiagobaptista.gazeustictactoe.model.Cell;
import com.thiagobaptista.gazeustictactoe.model.GameBoard;

import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;
import static com.thiagobaptista.gazeustictactoe.model.GameBoard.*;

public class GameBoardUnitTest
{
    private GameBoard gameBoard;

    private Cell[] cellPool = new Cell[9];

    @Before
    public void setup()
    {
        resetGameBoard();

        // Arranged so it resolves as a draw
        cellPool[0] = new Cell(0, 0);
        cellPool[1] = new Cell(1, 0);
        cellPool[2] = new Cell(0, 1);
        cellPool[3] = new Cell(0, 2);
        cellPool[4] = new Cell(1, 1);
        cellPool[5] = new Cell(2, 1);
        cellPool[6] = new Cell(1, 2);
        cellPool[7] = new Cell(2, 2);
        cellPool[8] = new Cell(2, 0);
    }

    private void resetGameBoard()
    {
        gameBoard = new GameBoard();
        gameBoard.setGameBoardListener( new MockListener() );
    }

    @Test
    public void should_game_start_in_IN_PROGRESS_state()
    {
        assertEquals(GameState.IN_PROGRESS, gameBoard.getCurrentState());
    }

    @Test
    public void should_game_start_with_no_winner()
    {
        assertEquals(Player.NONE, gameBoard.getWinner());
    }

    @Test
    public void should_cells_in_the_board_be_empty_at_the_start_of_the_game()
    {
        assert checkAllCellsEmpty();
    }

    private boolean checkAllCellsEmpty()
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                Player mark = gameBoard.checkCell(new Cell(i, j));
                if (mark != Player.NONE)
                {
                    return false;
                }
            }
        }
        return true;
    }

    @Test
    public void should_current_player_alternate_between_moves()
    {
        int numberOfMoves = new Random().nextInt(9) + 1;
        Player expectedPlayer;

        for (int i = 0; i < numberOfMoves; i++)
        {
            gameBoard.makeAMove(cellPool[i]);
        }

        if (numberOfMoves % 2 == 0)
        {
            expectedPlayer = Player.X;
        }
        else
        {
            expectedPlayer = Player.O;
        }

        assertEquals(expectedPlayer, gameBoard.getCurrentPlayer());
    }

    @Test
    public void should_the_current_turn_counter_increase_accordingly()
    {
        int numberOfMoves = new Random().nextInt(9) + 1;

        for (int i = 0; i < numberOfMoves; i++)
        {
            gameBoard.makeAMove(cellPool[i]);
        }

        // Turn counter starts at 1
        assertEquals(numberOfMoves, gameBoard.getCurrentTurn());
    }

    @Test
    public void should_game_end_after_nine_moves_and_be_a_draw()
    {
        for (int i = 0; i < 9; i++)
        {
            gameBoard.makeAMove(cellPool[i]);
        }

        assertEquals(GameState.GAME_OVER, gameBoard.getCurrentState());
        assertEquals(Player.NONE, gameBoard.getWinner());
    }

    @Test
    public void should_game_not_progress_if_player_X_marks_the_same_cell_twice()
    {
        Cell cell = new Cell(1, 2);
        // If it was a valid move, by the time player X plays for the third time,
        // it should be turn number 4 (and it should be player O's turn).
        int expectedTurn = 2;

        // X plays
        gameBoard.makeAMove(cell);
        // O plays
        gameBoard.makeAMove(new Cell(0, 0));
        // X plays
        gameBoard.makeAMove(cell);
        // X plays again, NOT player O
        gameBoard.makeAMove(cell);

        assertEquals(expectedTurn, gameBoard.getCurrentTurn());
    }

    @Test
    public void should_game_not_switch_players_if_player_X_marks_the_same_cell_twice()
    {
        Cell cell = new Cell(1, 2);

        // X plays
        gameBoard.makeAMove(cell);
        // O plays
        gameBoard.makeAMove(new Cell(0, 0));
        // X plays, but next player REMAINS X
        gameBoard.makeAMove(cell);

        assertEquals(Player.X, gameBoard.getCurrentPlayer());
    }

    @Test
    public void should_game_not_progress_if_player_O_marks_the_same_cell_twice()
    {
        Cell cell = new Cell(1, 2);
        // If it was a valid move, by the time player O plays for the third time,
        // it should be turn number 5 (and it should be player X's turn).
        int expectedTurn = 3;

        // X plays
        gameBoard.makeAMove(new Cell(0, 0));
        // O plays
        gameBoard.makeAMove(cell);
        // X plays
        gameBoard.makeAMove(new Cell(0, 1));
        // O plays
        gameBoard.makeAMove(cell);
        // O plays again, NOT player X
        gameBoard.makeAMove(cell);

        assertEquals(expectedTurn, gameBoard.getCurrentTurn());
    }

    @Test
    public void should_game_not_switch_players_if_player_O_marks_the_same_cell_twice()
    {
        Cell cell = new Cell(1, 2);

        // X plays
        gameBoard.makeAMove(new Cell(0, 0));
        // O plays
        gameBoard.makeAMove(cell);
        // X plays
        gameBoard.makeAMove(new Cell(0, 1));
        // O plays, but next player REMAINS O
        gameBoard.makeAMove(cell);

        assertEquals(Player.O, gameBoard.getCurrentPlayer());
    }

    @Test
    public void should_game_not_progress_if_two_players_mark_the_same_cell()
    {
        Cell cell = new Cell(1, 2);
        // If it was a valid move, it would be turn number 2, and the current player
        // would switch to player X.
        int expectedTurn = 1;

        // X plays
        gameBoard.makeAMove(cell);
        // O plays
        gameBoard.makeAMove(cell);

        assertEquals(expectedTurn, gameBoard.getCurrentTurn());
    }

    @Test
    public void should_game_not_switch_players_if_two_players_mark_the_same_cell()
    {
        Cell cell = new Cell(1, 2);

        // X plays
        gameBoard.makeAMove(cell);
        // O plays, but next player REMAINS O
        gameBoard.makeAMove(cell);

        assertEquals(Player.O, gameBoard.getCurrentPlayer());
    }

    @Test
    public void should_game_end_by_player_X_completing_a_row()
    {
        playerXWinsByCompletingARow();

        assertEquals(GameState.GAME_OVER, gameBoard.getCurrentState());
        assertEquals(Player.X, gameBoard.getWinner());
    }

    private void playerXWinsByCompletingARow()
    {
        // X plays
        gameBoard.makeAMove(new Cell(0, 0));
        // O plays
        gameBoard.makeAMove(new Cell(2, 0));

        // X plays
        gameBoard.makeAMove(new Cell(0, 1));
        // O plays
        gameBoard.makeAMove(new Cell(2, 2));

        // X plays
        gameBoard.makeAMove(new Cell(0, 2));
    }

    @Test
    public void should_game_end_by_player_O_completing_a_row()
    {
        playerOWinsByCompletingARow();

        assertEquals(GameState.GAME_OVER, gameBoard.getCurrentState());
        assertEquals(Player.O, gameBoard.getWinner());
    }

    private void playerOWinsByCompletingARow()
    {
        // X plays
        gameBoard.makeAMove(new Cell(2, 0));
        // O plays
        gameBoard.makeAMove(new Cell(0, 0));

        // X plays
        gameBoard.makeAMove(new Cell(2, 2));
        // O plays
        gameBoard.makeAMove(new Cell(0, 1));

        // X plays
        gameBoard.makeAMove(new Cell(1, 1));
        // O plays
        gameBoard.makeAMove(new Cell(0, 2));
    }

    @Test
    public void should_game_end_by_player_X_completing_a_column()
    {
        playerXWinsByCompletingAColumn();

        assertEquals(GameState.GAME_OVER, gameBoard.getCurrentState());
        assertEquals(Player.X, gameBoard.getWinner());
    }

    private void playerXWinsByCompletingAColumn()
    {
        // X plays
        gameBoard.makeAMove(new Cell(0, 0));
        // O plays
        gameBoard.makeAMove(new Cell(0, 2));

        // X plays
        gameBoard.makeAMove(new Cell(1, 0));
        // O plays
        gameBoard.makeAMove(new Cell(1, 2));

        // X plays
        gameBoard.makeAMove(new Cell(2, 0));
    }

    @Test
    public void should_game_end_by_player_O_completing_a_column()
    {
        playerOWinsByCompletingAColumn();

        assertEquals(GameState.GAME_OVER, gameBoard.getCurrentState());
        assertEquals(Player.O, gameBoard.getWinner());
    }

    private void playerOWinsByCompletingAColumn()
    {
        // X plays
        gameBoard.makeAMove(new Cell(0, 2));
        // O plays
        gameBoard.makeAMove(new Cell(0, 0));

        // X plays
        gameBoard.makeAMove(new Cell(1, 2));
        // O plays
        gameBoard.makeAMove(new Cell(1, 0));

        // X plays
        gameBoard.makeAMove(new Cell(1, 1));
        // O plays
        gameBoard.makeAMove(new Cell(2, 0));
    }

    @Test
    public void should_game_end_by_player_X_completing_a_downward_diagonal()
    {
        playerXWinsByCompletingADownwardDiagonal();

        assertEquals(GameState.GAME_OVER, gameBoard.getCurrentState());
        assertEquals(Player.X, gameBoard.getWinner());
    }

    private void playerXWinsByCompletingADownwardDiagonal()
    {
        // X plays
        gameBoard.makeAMove(new Cell(0, 0));
        // O plays
        gameBoard.makeAMove(new Cell(0, 2));

        // X plays
        gameBoard.makeAMove(new Cell(1, 1));
        // O plays
        gameBoard.makeAMove(new Cell(1, 2));

        // X plays
        gameBoard.makeAMove(new Cell(2, 2));
    }

    @Test
    public void should_game_end_by_player_O_completing_a_downward_diagonal()
    {
        playerOWinsByCompletingADownwardDiagonal();

        assertEquals(GameState.GAME_OVER, gameBoard.getCurrentState());
        assertEquals(Player.O, gameBoard.getWinner());
    }

    private void playerOWinsByCompletingADownwardDiagonal()
    {
        // X plays
        gameBoard.makeAMove(new Cell(0, 2));
        // O plays
        gameBoard.makeAMove(new Cell(0, 0));

        // X plays
        gameBoard.makeAMove(new Cell(1, 2));
        // O plays
        gameBoard.makeAMove(new Cell(1, 1));

        // X plays
        gameBoard.makeAMove(new Cell(2, 0));
        // O plays
        gameBoard.makeAMove(new Cell(2, 2));
    }

    @Test
    public void should_game_end_by_player_X_completing_a_upward_diagonal()
    {
        playerXWinsByCompletingAUpwardDiagonal();

        assertEquals(GameState.GAME_OVER, gameBoard.getCurrentState());
        assertEquals(Player.X, gameBoard.getWinner());
    }

    private void playerXWinsByCompletingAUpwardDiagonal()
    {
        // X plays
        gameBoard.makeAMove(new Cell(2, 0));
        // O plays
        gameBoard.makeAMove(new Cell(0, 0));

        // X plays
        gameBoard.makeAMove(new Cell(1, 1));
        // O plays
        gameBoard.makeAMove(new Cell(1, 2));

        // X plays
        gameBoard.makeAMove(new Cell(0, 2));
    }

    @Test
    public void should_game_end_by_player_O_completing_a_upward_diagonal()
    {
        playerOWinsByCompletingAUpwardDiagonal();

        assertEquals(GameState.GAME_OVER, gameBoard.getCurrentState());
        assertEquals(Player.O, gameBoard.getWinner());
    }

    private void playerOWinsByCompletingAUpwardDiagonal()
    {
        // X plays
        gameBoard.makeAMove(new Cell(0, 0));
        // O plays
        gameBoard.makeAMove(new Cell(2, 0));

        // X plays
        gameBoard.makeAMove(new Cell(1, 0));
        // O plays
        gameBoard.makeAMove(new Cell(1, 1));

        // X plays
        gameBoard.makeAMove(new Cell(2, 2));
        // O plays
        gameBoard.makeAMove(new Cell(0, 2));
    }

    @Test
    public void should_game_not_progress_if_it_is_in_GAME_OVER_state()
    {
        int expectedNumberOfMovesForXToWin = 5;
        int expectedNumberOfMovesForOToWin = 6;

        playerOWinsByCompletingAColumn();
        gameBoard.makeAMove(new Cell(2,1));
        assertEquals(expectedNumberOfMovesForOToWin, gameBoard.getCurrentTurn());

        resetGameBoard();
        playerOWinsByCompletingARow();
        gameBoard.makeAMove(new Cell(2,1));
        assertEquals(expectedNumberOfMovesForOToWin, gameBoard.getCurrentTurn());

        resetGameBoard();
        playerOWinsByCompletingADownwardDiagonal();
        gameBoard.makeAMove(new Cell(2,1));
        assertEquals(expectedNumberOfMovesForOToWin, gameBoard.getCurrentTurn());

        resetGameBoard();
        playerOWinsByCompletingAUpwardDiagonal();
        gameBoard.makeAMove(new Cell(2,1));
        assertEquals(expectedNumberOfMovesForOToWin, gameBoard.getCurrentTurn());

        resetGameBoard();
        playerXWinsByCompletingAColumn();
        gameBoard.makeAMove(new Cell(2,1));
        assertEquals(expectedNumberOfMovesForXToWin, gameBoard.getCurrentTurn());

        resetGameBoard();
        playerXWinsByCompletingARow();
        gameBoard.makeAMove(new Cell(2,1));
        assertEquals(expectedNumberOfMovesForXToWin, gameBoard.getCurrentTurn());

        resetGameBoard();
        playerXWinsByCompletingADownwardDiagonal();
        gameBoard.makeAMove(new Cell(2,1));
        assertEquals(expectedNumberOfMovesForXToWin, gameBoard.getCurrentTurn());

        resetGameBoard();
        playerXWinsByCompletingAUpwardDiagonal();
        gameBoard.makeAMove(new Cell(2,1));
        assertEquals(expectedNumberOfMovesForXToWin, gameBoard.getCurrentTurn());
    }

    private class MockListener implements GameBoardListener
    {
        @Override
        public void onCellMarked(Cell cell, Player mark)
        {
            // do nothing
        }

        @Override
        public void onPlayerSwitched(Player currentPlayer)
        {
            // Do nothing
        }

        @Override
        public void onPlayerWin(Player winner, List<Cell> winningCells)
        {
            // Do nothing
        }

        @Override
        public void onDraw()
        {
            // Do nothing
        }
    }
}